**<font size="5">OPENGAUSS.O_6.G_1.R_11：确保没有host条目的数据库指定为all</font>**

**配置组ID:**
OPENGAUSS.G_1

**配置组名称:**
连接配置

**规则ID:**
OPENGAUSS.O_6.G_1.R_11

**规则名称:**
确保没有host条目的数据库指定为all

**级别:**
建议

**相关要求:**
无

**规则背景说明:**

host连接条目若指定数据库为all，则允许用户连接到任何一个数据库。在满足业务需求的前提下，建议为不同的用户或客户端IP指定所需连接的数据库，避免不同业务之间相互影响。

**影响:**
无

**检查方法:**

通过如下shell命令检查`pg_hba.conf`配置中是否存在数据库指定为all的条目，其中`${GAUSSDATA}`为DN的数据目录。此检查需要人工确认哪些是服务端节点IP和本地环回IP（如127.0.0.1和::1），确保除服务端节点IP和本地环回IP外无host条目的数据库指定为all。

```bash
grep -P '^[^#]*host(ssl|nossl)?\s+[Aa][Ll][Ll]' ${GAUSSDATA}/pg_hba.conf
```

**修复方法:**

除服务端内部连接外，根据业务需要将配置文件`pg_hba.conf`中host、hostssl条目中的数据库为all的条目修改为需要连接的数据库。

**参考来源:**
无